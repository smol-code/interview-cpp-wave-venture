cmake_minimum_required(VERSION 3.25)
project(test-wave-venture CXX)


# Default to debug
if(NOT CMAKE_BUILD_TYPE)
	set(CMAKE_BUILD_TYPE Debug)
endif()



# NOTE: "temporarily" set to C++17
set(CMAKE_CXX_STANDARD 17)

# include(CheckIncludeFileCXX)
# check_include_file_cxx(any HAS_ANY)
# check_include_file_cxx(string_view HAS_STRING_VIEW)
# check_include_file_cxx(coroutine HAS_COROUTINE)
# set(CMAKE_CXX_STANDARD_REQUIRED ON)
# set(CMAKE_CXX_EXTENSIONS OFF)

add_compile_options(-Wall)
add_compile_options(-Wextra)

# Address Sanitizer in debug mode
if (CMAKE_BUILD_TYPE MATCHES "Debug")
	message("-----------------------------------------------------------------------------")
	message("Using Address Sanitizer for memory errors")
	message("-----------------------------------------------------------------------------")
	# Set flag for nicer stack messages (idk what diff it makes)
	add_compile_options(-fno-omit-frame-pointer)
	add_link_options(-fno-omit-frame-pointer)
	add_compile_options(-fno-optimize-sibling-calls)
	add_link_options(-fno-optimize-sibling-calls)

	# Add to all targets
	add_compile_options(-fsanitize=address)
	add_link_options(-fsanitize=address)
	# target_compile_options(${PROJECT_NAME} PRIVATE -fsanitize=address)
	# target_link_options(${PROJECT_NAME} PRIVATE -fsanitize=address)
endif (CMAKE_BUILD_TYPE MATCHES "Debug")



# WARN: Using this requires a CMake re-config when adding/removing files,
# and I think it might not be required
# aux_source_directory(src SRC_MAIN)


add_executable(${PROJECT_NAME} src/main.cc)



# -----------------------------------------------------------------------------
# NOTE: This is taken from my project that uses drogon
# Run clang-tidy on normal build
set(CMAKE_CXX_CLANG_TIDY clang-tidy)
# Run run-clang-tidy python script as a custom command that uses
# compile_commands.json and intentionally does not run during "ALL" build
add_custom_target(lint-tidy)
# add_custom_command(TARGET lint-tidy POST_BUILD COMMAND run-clang-tidy -header-filter='^.*(?:third_party).*')
add_custom_command(TARGET lint-tidy POST_BUILD COMMAND
	run-clang-tidy -quiet src* > ../clang-tidy-out.txt &&
	cat ../clang-tidy-out.txt &&
	echo &&
	echo &&
	echo &&
	echo "-----------------------------------------------------------------------------" &&
	echo "See ./clang-tidy-out.txt for output" &&
	echo "-----------------------------------------------------------------------------" &&
	echo &&
	echo &&
	echo
)
# -----------------------------------------------------------------------------



target_include_directories(
	${PROJECT_NAME}
	SYSTEM
	PRIVATE include
)

# target_link_directories(${PROJECT_NAME} PUBLIC third_party/phc-winner-argon2-20190702)

target_link_libraries(
	${PROJECT_NAME}
	# System libraries
	# PRIVATE none
	# Third party installed
	# PRIVATE none
	# Third party local
	# PRIVATE none
	# Internal libraries
	# PRIVATE none
)

# target_sources(
# 	${PROJECT_NAME}
# 	PRIVATE
# 	${SRC_MAIN}
# )
