## std::vector vs union of std::array
I've been playing around with unions a little (implementing a naive std::optional in my other project) and felt that it may be faster to have a union of two possble arrays rather than a vector.

I'm mostly just appeasing my curiosity.

Source code: [vec-vs-union.cc](./vec-vs-union.cc)

On compiler explorer: https://godbolt.org/z/xn4P7Y8qb

In debug mode the array union was faster
![](./vec-union-timing-debug.png)

But in release mode it was only technically faster
![](./vec-union-timing-release.png)


The compiler just optimised it away, so in a real-world non-constexpr use-case both would probably be non-zero, with the union being a tiny bit faster?
