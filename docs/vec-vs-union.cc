#include <array>
#include <chrono>
#include <iostream>
#include <vector>

union oneortwo {
	std::array<int, 1> one;
	std::array<int, 2> two;

	oneortwo(int a)
		: one({ a })
	{
	}
	oneortwo(int a, int b)
		: two({ a, b })
	{
	}
};

auto implunion1()
{
	oneortwo retval { 1 };
	return retval;
}
auto implunion2()
{
	oneortwo retval { 1, 2 };
	return retval;
}

auto implvec1()
{
	std::vector<int> retval { 1 };
	return retval;
}
auto implvec2()
{
	std::vector<int> retval { 1, 2 };
	return retval;
}



constexpr int iteration_count = 1000000;

void testunion()
{
	int total = 0;

	using std::chrono::duration;
	using std::chrono::duration_cast;
	using std::chrono::high_resolution_clock;
	using std::chrono::milliseconds;

	auto t1 = high_resolution_clock::now();
	for (int i = 0; i != iteration_count; ++i) {
		auto ret = implunion1();
		total += ret.one[0];
	}
	for (int i = 0; i != iteration_count; ++i) {
		auto ret = implunion2();
		total += ret.two[0];
	}
	auto t2 = high_resolution_clock::now();

	/* Getting number of milliseconds as an integer. */
	auto ms_int = duration_cast<milliseconds>(t2 - t1);

	std::cout << "total: " << total << '\n';
	std::cout << "time union: " << ms_int.count() << '\n';
}


void testvector()
{
	int total = 0;

	using std::chrono::duration;
	using std::chrono::duration_cast;
	using std::chrono::high_resolution_clock;
	using std::chrono::milliseconds;

	auto t1 = high_resolution_clock::now();
	for (int i = 0; i != iteration_count; ++i) {
		auto ret = implvec1();
		total += ret[0];
	}
	for (int i = 0; i != iteration_count; ++i) {
		auto ret = implvec2();
		total += ret[0];
	}
	auto t2 = high_resolution_clock::now();

	/* Getting number of milliseconds as an integer. */
	auto ms_int = duration_cast<milliseconds>(t2 - t1);
	std::cout << "total: " << total << '\n';
	std::cout << "time vector: " << ms_int.count() << '\n';
}

// int main() {
// 	testunion();
// 	testvector();
// 	testunion();
// 	testvector();
// }