from pxyTools import JSONDict

data = JSONDict("sample_challenge_data.json")
data_dry = JSONDict("ai1-dry.json")
data_wet = JSONDict("ai1-wet.json")

quadrilaterals = data["q"]
points = data["p"]

data_dry["q"] = []
data_dry["p"] = []
dry_quadrilaterals = data_dry["q"]
dry_points = data_dry["p"]

data_wet["q"] = []
data_wet["p"] = []
wet_quadrilaterals = data_wet["q"]
wet_points = data_wet["p"]


def split_line_zero(pos_x1, pos_y1, pos_z1, pos_x2, pos_y2, pos_z2):
    if ((pos_z1 >= 0 > pos_z2) or (pos_z2 >= 0 > pos_z1)) and pos_z2 - pos_z1 != 0:
        quad_line_scalar = abs((0 - pos_z1) / (pos_z2 - pos_z1))
        quad_line_x = pos_x1 + quad_line_scalar*(pos_x2 - pos_x1)
        quad_line_y = pos_y1 + quad_line_scalar*(pos_y2 - pos_y1)
        quad_line_parts = [((pos_x1, pos_y1, pos_z1), (quad_line_x, quad_line_y, 0.0)),
                           ((quad_line_x, quad_line_y, 0.0), (pos_x2, pos_y2, pos_z2))]
        return quad_line_parts
    else:
        quad_line_parts = [((pos_x1, pos_y1, pos_z1), (pos_x2, pos_y2, pos_z2))]
        return quad_line_parts


def extract_point(lines_parts):
    """Extract points and form 2 new quadrilaterals"""
    quad_pos_points = []
    quad_neg_points = []

    for line_parts in lines_parts:
        for line_part in line_parts:
            for line_point in line_part:
                lp_x, lp_y, lp_z = line_point

                if line_point not in quad_pos_points:
                    if lp_z >= 0:
                        quad_pos_points.append(line_point)

                if line_point not in quad_neg_points:
                    if lp_z <= 0:
                        quad_neg_points.append(line_point)

    print("pos count: ", len(quad_pos_points))
    print("neg count: ", len(quad_neg_points))
    return quad_pos_points, quad_neg_points


# Returns the quadrilateral with the x/y/z of each point loaded
def load_quadrilateral(point_list, quad):
    x1, y1, z1 = point_list[quad[0]]
    x2, y2, z2 = point_list[quad[1]]
    x3, y3, z3 = point_list[quad[2]]
    x4, y4, z4 = point_list[quad[3]]
    return [(x1, y1, z1), (x2, y2, z2), (x3, y3, z3), (x4, y4, z4)]


def dump_quadrilateral(quad_list, point_list, quad):
    resolved_quad = []
    for x, y, z in quad:
        coord = [x, y, z]
        if coord not in point_list:
            point_list.append(coord)

        resolved_quad.append(point_list.index(coord))

    print(resolved_quad)

    if resolved_quad not in quad_list:
        quad_list.append(resolved_quad)


def process_quadrilateral(quad_points):
    x1, y1, z1 = quad_points[0]
    x2, y2, z2 = quad_points[1]
    x3, y3, z3 = quad_points[2]
    x4, y4, z4 = quad_points[3]

    if z1 >= 0 and z2 >= 0 and z3 >= 0 and z4 >= 0:
        return quad_points, None
    elif z1 < 0 and z2 < 0 and z3 < 0 and z4 < 0:
        return None, quad_points
    else:
        quad_line1_parts = split_line_zero(x1, y1, z1, x2, y2, z2)
        quad_line2_parts = split_line_zero(x2, y2, z2, x3, y3, z3)
        quad_line3_parts = split_line_zero(x3, y3, z3, x4, y3, z4)
        quad_line4_parts = split_line_zero(x4, y4, z4, x1, y1, z1)

        quad_positive, quad_negative = extract_point([quad_line1_parts, quad_line2_parts,
                                                      quad_line3_parts, quad_line4_parts])
        return quad_positive, quad_negative


for q in quadrilaterals:
    dry, wet = process_quadrilateral(load_quadrilateral(points, q))
    if dry:
        dump_quadrilateral(wet_quadrilaterals, wet_points, dry)
    if wet:
        dump_quadrilateral(dry_quadrilaterals, dry_points, wet)

data_dry.save()
data_wet.save()
