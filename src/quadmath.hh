#pragma once

#include <cassert>
#include <cstdint>
#include <iostream>
#include <stdexcept>
#include <vector>

#include "types_quad.hh"



namespace quad_math {

// Represents the post-decosntruction points of the quad,
// returned from extract_point.
//
// More than 4 points may be returned, hence vector not array
struct quad_dry_wet_points {
	std::vector<typez::quad::point_xyz> dry;
	std::vector<typez::quad::point_xyz> wet;
};

// A line that makes up a quad.
// Represents the start and end 3D positions of the line
struct line_3d {
	typez::quad::point_xyz start;
	typez::quad::point_xyz end;
};

// Union of two arrays that holds one or two lines.
//
// NOTE: I used a union instead of a vector to make it obvious they differ,
// and for fun because std::array > std::vector in perf.
// And as long as you check is_intersected, you get some safety
// with out-of-bounds access.
//
// See: ../docs/vec-vs-union-array.md
// See: https://godbolt.org/z/xn4P7Y8qb
union one_or_two_lines {
	std::array<line_3d, 1> line_unintersected;
	std::array<line_3d, 2> lines_intersected;

	explicit one_or_two_lines(typez::quad::point_xyz start_line1,
		typez::quad::point_xyz end_line1,
		typez::quad::point_xyz start_line2,
		typez::quad::point_xyz end_line2)
		: lines_intersected()
	{
		lines_intersected[0].start = start_line1;
		lines_intersected[0].end = end_line1;
		lines_intersected[1].start = start_line2;
		lines_intersected[1].end = end_line2;
	}

	explicit one_or_two_lines(typez::quad::point_xyz start_line1, typez::quad::point_xyz end_line1)
		: line_unintersected()
	{
		line_unintersected[0].start = start_line1;
		line_unintersected[0].end = end_line1;
	}
};

// This holds either one line or two lines, based on is_intersected
// or four line parts when z does intersect 0.
//
// This is used to differentiate is a line has been split (intersected 0)
// or not.
struct intersected_lines {

	// We don't trust the user to set is_intersected so construct based on params
	intersected_lines(typez::quad::point_xyz start_line1,
		typez::quad::point_xyz end_line1,
		typez::quad::point_xyz start_line2,
		typez::quad::point_xyz end_line2)
		: is_intersected(true)
		, lines(start_line1, end_line1, start_line2, end_line2)
	{
	}

	// is_intersected is default false
	intersected_lines(typez::quad::point_xyz start_line1, typez::quad::point_xyz end_line1)
		: lines(start_line1, end_line1)
	{
	}

	// Assert for sanity. I'm not sure how I'd do this in production code
	// that's not my own personal project, but then I'd probably just use a vector.
	[[nodiscard]] auto get_intersected() const
	{
		assert(is_intersected == true);
		return lines.lines_intersected;
	}

	[[nodiscard]] auto get_unintersected() const
	{
		assert(is_intersected == false);
		return lines.line_unintersected;
	}

	[[nodiscard]] auto get_is_intersected() const { return is_intersected; }

private:
	// Intersected true == 2 lines. False = 1 line
	bool is_intersected { false };
	one_or_two_lines lines;
};

// Responsible for calculating whether the quad is dry (partially/fully) or wet (partially/fully).
//
// Usage:
// - `typez::quad::processed_dry_and_wet_quads reconstructed_quads;`
// - `quad_math::calculate_quad::process_quadrilateral(quad, reconstructed_quads);`
//
// Assumes output dry/wet can have varying number of points.
//
// TODO: Move impl to .cc file
class calculate_quad {

private:
	// Split the quadrilateral's lines when they intersect z=0.
	// WARNING: User must check against is_intersected
	[[nodiscard]] static inline auto split_line_zero(typez::quad::point_xyz points1, typez::quad::point_xyz points2)
		-> intersected_lines
	{
		if (
			// Check if the z axis encounters zero at any given point
			((points1.z >= 0 && 0 > points2.z) || (points2.z >= 0 && 0 > points1.z)) &&
			// And check if the z axis' differ
			// (If z is the same, then the line doesn't need splitting, even if it's at z=0)
			(points2.z - points1.z != 0)) {
			auto quad_line_scalar = std::abs((0 - points1.z) / (points2.z - points1.z));
			auto quad_line_x = points1.x + quad_line_scalar * (points2.x - points1.x);
			auto quad_line_y = points1.y + quad_line_scalar * (points2.y - points1.y);

			// std::cout << "SPLIT LINE\n";
			intersected_lines ret_lines_2 { // line 1 start
				points1,
				// line 1 end
				{ .x = quad_line_x, .y = quad_line_y, .z = 0 },
				// line 2 start(
				{ .x = quad_line_x, .y = quad_line_y, .z = 0 },
				// line 2 end
				points2
			};
			return ret_lines_2;
		}

		// Line wasn't split, reusing the given co-ords
		intersected_lines ret_lines_1 { // line 1 start
			points1,
			// line 1 end
			points2
		};

		// std::cout << "no split line\n";
		return ret_lines_1;
	}

	// Checks if the new_line exists within existing_quads_points
	//
	// REVIEW: clang-tidy suggests using `std::any_of()`
	// but I think std::find() with a lambda could be used also?
	[[nodiscard]] static inline auto are_line_parts_already_in_quad_points(
		const std::vector<typez::quad::point_xyz> &existing_quads_points, const typez::quad::point_xyz &new_line)
		-> bool
	{
		for (const auto &existing : existing_quads_points) {
			if (existing.x == new_line.x && existing.y == new_line.y && existing.z == new_line.z) {
				return true;
			}
		}
		return false;
	}

	// Get the index where new_line exists within existing_quads_points
	//
	// Use this when you know that the new_line does exist within existing_quads_points
	[[nodiscard]] static inline auto find_line_part_idx_in_quad_points(
		const std::vector<typez::quad::point_xyz> &existing_quads_points, const typez::quad::point_xyz &new_line)
		-> std::int64_t
	{
		std::int64_t idx = 0;
		for (const auto &existing : existing_quads_points) {
			if (existing.x == new_line.x && existing.y == new_line.y && existing.z == new_line.z) {
				return idx;
			}
			idx++;
		}
		throw std::invalid_argument("find_line_part_idx_in_quad_points requires that new_line exists in "
									"existing_quads_points. Use either the std::optional version or check your code.");
	}

	// Get the index where new_line exists within existing_quads_points
	//
	// std::optional is probably overkill and slow here, but I'm not using this version anyway
	[[nodiscard]] static inline auto find_line_part_idx_maybe_in_quad_points(
		const std::vector<typez::quad::point_xyz> &existing_quads_points, const typez::quad::point_xyz &new_line)
		-> std::optional<std::int64_t>
	{
		std::int64_t idx = 0;
		for (const auto &existing : existing_quads_points) {
			if (existing.x == new_line.x && existing.y == new_line.y && existing.z == new_line.z) {
				return idx;
			}
			idx++;
		}
		return std::nullopt;
	}

	// This is the inner portion of the loop in extract_point.
	// This is separated to reduce clutter
	//
	// Modifies reference
	static inline void extract_point_inner_loop(std::vector<typez::quad::point_xyz> &quad_pos_points,
		std::vector<typez::quad::point_xyz> &quad_neg_points,
		line_3d line_parts)
	{
		// Before any poitns are added, we check they don't already exist in their respective vector.
		// The z check is done first as it's faster.
		// I've not benchmarked but I've reasoned it as "fail fast"

		// Check if either start/end point is dry
		if (line_parts.start.z >= 0) {
			if (!are_line_parts_already_in_quad_points(quad_pos_points, line_parts.start)) {
				quad_pos_points.emplace_back(line_parts.start);
			}
		}
		if (line_parts.end.z >= 0) {
			if (!are_line_parts_already_in_quad_points(quad_pos_points, line_parts.end)) {
				quad_pos_points.emplace_back(line_parts.end);
			}
		}

		// Check if either start/end point is wet
		if (line_parts.start.z <= 0) {
			if (!are_line_parts_already_in_quad_points(quad_neg_points, line_parts.start)) {
				quad_neg_points.emplace_back(line_parts.start);
			}
		}
		if (line_parts.end.z <= 0) {
			if (!are_line_parts_already_in_quad_points(quad_neg_points, line_parts.end)) {
				quad_neg_points.emplace_back(line_parts.end);
			}
		}
	}

	// Extract the points and form 2 new "quadrilaterals"
	//
	// The number of points may exceed 4, so "quad" naming is deceiving.
	//
	// inline auto extract_point(std::array<intersected_lines, 4> multi_intersected_lines) ->
	// typez::quad::dry_andor_wet_quad {
	[[nodiscard]] static inline auto extract_point(std::array<intersected_lines, 4> multi_intersected_lines)
		-> quad_dry_wet_points
	{
		std::vector<typez::quad::point_xyz> quad_pos_points;
		std::vector<typez::quad::point_xyz> quad_neg_points;

		for (const auto &line_parts : multi_intersected_lines) {

			if (line_parts.get_is_intersected()) {
				for (const auto &line_parts : line_parts.get_intersected()) {
					extract_point_inner_loop(quad_pos_points, quad_neg_points, line_parts);
				}
			}
			else {
				for (const auto &line_parts : line_parts.get_unintersected()) {
					extract_point_inner_loop(quad_pos_points, quad_neg_points, line_parts);
				}
			}
		}

		return { .dry = quad_pos_points, .wet = quad_neg_points };
	}

	// This takes either the dry or wet "quads" and adds them to the aggregate.
	//
	// - Modifies reconstructed_quads by reference
	// - This handles one quad at a time.
	// - reconstructed_quads.processed_points indexes are re-used in the processed_quads
	// or inserted if they don't exist
	static inline void aggregate_quad(
		const std::vector<typez::quad::point_xyz> &decon_quad, typez::quad::processed_quads &reconstructed_quads)
	{
		// This holds the points of the reconstructed quad
		// Using vector as this may have more than 4 points
		std::vector<std::uint64_t> temp_recon_quad {};

		for (const auto &quad_points : decon_quad) {
			// Check if we already have the x,y,z points in the processed_points - duplicates are unnecessary
			if (!are_line_parts_already_in_quad_points(reconstructed_quads.processed_points, quad_points)) {
				reconstructed_quads.processed_points.emplace_back(quad_points);
				// If the quad_points didn't exist in the processed_points, we know after insert
				// that the index we want is the last one
				auto quad_points_idx = reconstructed_quads.processed_points.size() - 1;
				temp_recon_quad.emplace_back(quad_points_idx);
			}
			else {
				auto quad_points_idx
					= find_line_part_idx_in_quad_points(reconstructed_quads.processed_points, quad_points);
				temp_recon_quad.emplace_back(quad_points_idx);
			}
		}

		// Default to true as this loop "disqualifies" rather than "qualifies"
		// temp_recon_quad to be added.
		bool should_add_quad = true;
		for (const auto &existing : reconstructed_quads.processed_quads) {
			bool all_points_match = true;

			// Check in case the existing quad and the temp_recon_quad
			// have different number of points
			if (existing.size() != temp_recon_quad.size()) {
				continue;
			}

			// Check that the dynamic amount of points in `existing` match `temp_recon_quad`
			int index_iter = 0;
			for (const auto &temp_recon_index : temp_recon_quad) {

				// Once a non-matching point is found, we don't need to check further points,
				// hence break
				if (temp_recon_index != existing[index_iter]) {
					all_points_match = false;
					break;
				}

				index_iter++;
			}

			if (all_points_match) {
				should_add_quad = false;
			}
		}

		if (should_add_quad) {
			reconstructed_quads.processed_quads.emplace_back(temp_recon_quad);
		}
	}


public:
	// Process the quads and return the quad either as-is if it's fully wet or dry,
	// or split it into the dry and wet parts then return those parts separately.
	//
	// TODO: If required make the "quads" (split quad with potentially >4 points) quads again by deconstructing them.
	//
	// Modifies reconstructed_quads by reference
	static inline auto process_quadrilateral(
		const typez::quad::quad_with_points &quad, typez::quad::processed_dry_and_wet_quads &reconstructed_quads)
	{

		// Quad is fully dry, i.e. above (or eq) z = 0
		if (quad[0].z >= 0 && quad[1].z >= 0 && quad[2].z >= 0 && quad[3].z >= 0) {
			aggregate_quad({ quad[0], quad[1], quad[2], quad[3] }, reconstructed_quads.dry);
			// return {quad, std::nullopt};
			return;
		}

		// Quad is fully wet, i.e. below z = 0
		if (quad[0].z < 0 && quad[1].z < 0 && quad[2].z < 0 && quad[3].z < 0) {
			aggregate_quad({ quad[0], quad[1], quad[2], quad[3] }, reconstructed_quads.wet);
			// return {std::nullopt, quad};
			return;
		}

		// Get all the lines of the quadrilateral, A->B, B->C, C->D, D->A (indexes: 0->1, 1->2, 2->3, 3->0)
		// And split them whenever they intersect Z = 0
		auto line_parts1 = split_line_zero(quad[0], quad[1]);
		auto line_parts2 = split_line_zero(quad[1], quad[2]);
		auto line_parts3 = split_line_zero(quad[2], quad[3]);
		auto line_parts4 = split_line_zero(quad[3], quad[0]);

		auto dry_wet_quad_points = extract_point({ line_parts1, line_parts2, line_parts3, line_parts4 });

		if (!dry_wet_quad_points.dry.empty()) {
			aggregate_quad(dry_wet_quad_points.dry, reconstructed_quads.dry);
		}

		if (!dry_wet_quad_points.wet.empty()) {
			aggregate_quad(dry_wet_quad_points.wet, reconstructed_quads.wet);
		}
	}
};

};
