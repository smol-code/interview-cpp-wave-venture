#pragma once

#include <array>
#include <cstdint>
#include <optional>
#include <vector>

// This stores the types used for the quads,
// both for reading/writing and processing.
//
// Typo is intentional
// TODO: Better name.
namespace typez::quad {


// Using a struct instead of array for readability
// TODO: Use long double or quad-precision? I don't think rapidjson even supports long double though.
// See: https://stackoverflow.com/a/15660031/13310905
struct point_xyz {
	double x;
	double y;
	double z;

	// Allow default construction
	// point_xyz() = default;
	// Allow passing in the x/y/z positionally
	// point_xyz(double c_x, double c_y, double c_z) : x(c_x), y(c_y), z(c_z) {}
};


// This represents the quads with the points as a struct.
// Using std::array as quads have 4 points.
// NOTE: This is probably very inefficient - the points
// are re-used and could be references??
using quad_with_points = std::array<point_xyz, 4>;

// This represents the quads with the indexes to the points.
// This is not in use but would be if the returned files must only contain quads,
// as would be the case in meshes (or split into triangles).
using quad_with_indexes = std::array<std::uint64_t, 4>;

// This represents the varying-count of points that the deconstructed
// quads can become.
using nonquad_with_indexes = std::vector<std::uint64_t>;


struct dry_andor_wet_quad {
	// TODO: Can probably make it cleaner/faster than using two optionals?
	std::optional<quad_with_points> dry;
	std::optional<quad_with_points> wet;
};


// This contains the processed dry or wet quads, and the points they need.
//
// Not using quad_with_points because we don't need convenient access to
// each quad's points, and having them split makes writing out easier.
struct processed_quads {
	// quad_with_indexes would be used if the return requires the dry and we parts
	// to strictly be quads. I'm assuming the number of co-ords may vary.
	std::vector<typez::quad::nonquad_with_indexes> processed_quads;
	std::vector<typez::quad::point_xyz> processed_points;
};


struct processed_dry_and_wet_quads {
	processed_quads dry;
	processed_quads wet;
};


};
