#pragma once

#include <array>
#include <fstream>
#include <iostream>
#include <optional>
#include <vector>

#include "rapidjson/document.h"
#include "rapidjson/reader.h"

#include "types_quad.hh"



namespace json_io {


// Parses the source data into a usable vector of quads with their x/y/z set.
//
// Using std::optional to stay sane, and because in a real code base there should be proper error handling.
// (incompletey) example in my other code:
// - https://gitlab.com/aterlux/drogon-home/-/blob/5fd494c4546aae0ce68245525726b1deb8f601c8/util/error.hh#L126
// - https://gitlab.com/aterlux/drogon-home/-/blob/5fd494c4546aae0ce68245525726b1deb8f601c8/util/response_status.hh#L86
// - https://gitlab.com/aterlux/drogon-home/-/blob/5fd494c4546aae0ce68245525726b1deb8f601c8/util/hash.hh#L93
inline auto parse_src_quads(const std::string &src_file = "../docs/sample_challenge_data.json")
	-> std::optional<std::vector<typez::quad::quad_with_points>>
{

	// using namespace typez::quad;

	std::ifstream in_json_file(src_file);

	// Read whole file. I don't think I've done it like this before lol
	std::string in_json((std::istreambuf_iterator<char>(in_json_file)), std::istreambuf_iterator<char>());

	rapidjson::Document json_doc;

	// For better precision could parse as string then figure out getting
	// long double or quad-precision.
	// json_doc.Parse<rapidjson::kParseNumbersAsStringsFlag>(json);
	json_doc.Parse<rapidjson::kParseFullPrecisionFlag>(in_json.c_str());

	if (json_doc.HasParseError()) {
		auto err = json_doc.GetParseError();
		std::cout << "[ERROR::parsing] rapidjson parse error: " << err << '\n';
		return std::nullopt;
	}

	rapidjson::Value &quad_arr = json_doc["q"];
	rapidjson::Value &point_arr = json_doc["p"];

	if (!quad_arr.IsArray()) {
		std::cout << "[ERROR::parsing]: \"q\" key is not an array. \n";
		return std::nullopt;
	}
	if (!point_arr.IsArray()) {
		std::cout << "[ERROR::parsing]: \"p\" key is not an array. \n";
		return std::nullopt;
	}

	std::vector<typez::quad::quad_with_points> parsed_quads;
	std::vector<typez::quad::point_xyz> original_points;
	// We know the size so can avoid the amortised O(1) resize on insert
	original_points.reserve(point_arr.Size());

	// `point` contains the array of [x, y, z] points
	for (const auto &points : point_arr.GetArray()) {
		// Basic error checking
		if (!points.IsArray()) {
			std::cout << "[ERROR::parsing]: member of \"p\" is not an array. \n";
			return std::nullopt;
		}
		if (points.Size() != 3) {
			std::cout << "[ERROR::parsing]: array member of \"p\" is not size 3 (x, y, z). \n";
			return std::nullopt;
		}
		if (!points[0].IsDouble() || !points[1].IsDouble() || !points[2].IsDouble()) {
			std::cout << "[ERROR::parsing]: member of \"p\" has a non-double point \n";
			return std::nullopt;
		}

		typez::quad::point_xyz new_point {};
		new_point.x = points[0].GetDouble();
		new_point.y = points[1].GetDouble();
		new_point.z = points[2].GetDouble();
		original_points.emplace_back(new_point);
	}

	// `quad` contains the array of indexes to the points
	// Equivalent to `quad_with_indexes`
	for (const auto &quad : quad_arr.GetArray()) {
		// Basic error checking
		if (!quad.IsArray()) {
			std::cout << "[ERROR::parsing]: member of \"q\" is not an array. \n";
			return std::nullopt;
		}
		if (quad.Size() != 4) {
			std::cout << "[ERROR::parsing]: array member of \"q\" is not size 4. \n";
			return std::nullopt;
		}
		if (!quad[0].IsInt64() || !quad[1].IsInt64() || !quad[2].IsInt64() || !quad[3].IsInt64()) {
			std::cout << "[ERROR::parsing]: member of \"q\" has a non-int index to a point \n";
			return std::nullopt;
		}

		// NOTE: Assumes that the points indexed in "q" array are 0-indexed. Which seem to be the case.
		typez::quad::quad_with_points new_parsed_quad {};
		new_parsed_quad[0] = original_points[quad[0].GetInt64()];
		new_parsed_quad[1] = original_points[quad[1].GetInt64()];
		new_parsed_quad[2] = original_points[quad[2].GetInt64()];
		new_parsed_quad[3] = original_points[quad[3].GetInt64()];
		parsed_quads.emplace_back(new_parsed_quad);
	}

	return parsed_quads;
}

};
