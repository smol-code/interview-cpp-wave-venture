#pragma once

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>

#include "rapidjson/stringbuffer.h"
#include "rapidjson/writer.h"

#include "types_quad.hh"


namespace json_io {



// Generates the JSON string of q and p.
//
// Usage:
// - `rapidjson::StringBuffer str_buf;`
// - `rapidjson::Writer<rapidjson::StringBuffer> writer(str_buf);`
//- `jsonify_quads(str_buf, writer, processed_quads.dry);
//
// NOTE: Does not clear str_buf; user shall manage that themselves:
// - str_buf.Clear();
// - writer.Reset(str_buf);
//
// Not class+private as reusing this could be re-used
// There's no use case for it now but it could be used outside of output_all_quads
// to use the JSON string in memory for something else, e.g. logging or in some API.
inline void jsonify_quads(
	// rapidjson::StringBuffer &str_buf,
	rapidjson::Writer<rapidjson::StringBuffer> &writer,
	const typez::quad::processed_quads &quads)
{
	// str_buf.Flush();
	// str_buf.Clear();

	writer.StartObject();

	writer.Key("q");
	writer.StartArray();
	// Array of arrays, as each quad has multiple indexes to `p` key
	for (const auto &quad_dry : quads.processed_quads) {
		writer.StartArray();
		for (const auto &quad_point_idx : quad_dry) {
			writer.Uint(quad_point_idx);
		}
		writer.EndArray();
	}
	writer.EndArray();

	writer.Key("p");
	writer.StartArray();
	// Array of arrays, each point has 3 entries
	for (const auto &point_dry : quads.processed_points) {
		writer.StartArray();
		writer.Double(point_dry.x);
		writer.Double(point_dry.y);
		writer.Double(point_dry.z);
		writer.EndArray();
	}
	writer.EndArray();

	writer.EndObject();
}

// Writes the dry and wet JSON files.
//
// TODO: Add option to beautify the output, which'd be taken as a CLI arg.
inline auto output_all_quads(const typez::quad::processed_dry_and_wet_quads &processed_quads,
	const std::string &out_filename_dry = "../output_dry.json",
	const std::string &out_filename_wet = "../output_wet.json") -> bool
{

	rapidjson::StringBuffer str_buf;
	rapidjson::Writer<rapidjson::StringBuffer> writer(str_buf);

	jsonify_quads(writer, processed_quads.dry);
	// std::cout << str_buf.GetString() << '\n';

	std::ofstream out_dry(out_filename_dry);
	out_dry << str_buf.GetString();
	out_dry.close();
	if (out_dry.bad()) {
		std::cerr << "[ERROR::writing_file]: output_all_quads failed writing out_dry to file\n";
		return false;
	}

	str_buf.Clear();
	writer.Reset(str_buf);

	jsonify_quads(writer, processed_quads.wet);

	std::ofstream out_wet(out_filename_wet);
	out_wet << str_buf.GetString();
	out_wet.close();
	if (out_wet.bad()) {
		std::cerr << "[ERROR::writing_file]: output_all_quads failed writing out_wet to file\n";
		return false;
	}


	return true;
}

};
