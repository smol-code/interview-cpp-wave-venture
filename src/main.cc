#include <cstdlib>
#include <iostream>

#include "rapidjson/document.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/writer.h"

#include "qreader.hh"
#include "quadmath.hh"
#include "qwriter.hh"
#include "types_quad.hh"


// This handles the reading and writing of the JSON files,
// whilst returning and receiving somewhat sane data types.
// namespace json_io {};

// This has the class that processes the quadrilaterals,
// and some supporting types.
//
// My use of unions here is "experimental"
// namespace quad_math {};


auto main() -> int
{

	auto quads = json_io::parse_src_quads();

	if (!quads.has_value()) {
		std::cerr << "[ERROR::main]: parse_src_quads failed\n";
		// FIXME: The below shouldn't be a failure case, but for the purpose of this code,
		// I think it's ok to not check if the current dir is build.
		std::cerr << "Tip: The locations of files are hardcoded assuming you are in the ./build/ dir. Please run "
					 "through CMake or cd into build\n";
		return EXIT_FAILURE;
	}

	auto quads_values = quads.value();

	std::vector<typez::quad::quad_with_points> dry_quads;
	std::vector<typez::quad::quad_with_points> wet_quads;

	// TODO: Benchmark reserving more than needed vs
	// letting the loop reserve
	dry_quads.reserve(quads_values.size());
	wet_quads.reserve(quads_values.size());

	// The dry and wet quads get processed then aggregated into this.
	// A reference was easier than passing around the existing vector
	// and outputting new entries to emplace_back here.
	typez::quad::processed_dry_and_wet_quads reconstructed_quads;

	for (const auto &quad : quads_values) {
		quad_math::calculate_quad::process_quadrilateral(quad, reconstructed_quads);
	}

	// Write files
	auto write_ok = json_io::output_all_quads(reconstructed_quads);

	if (!write_ok) {
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
