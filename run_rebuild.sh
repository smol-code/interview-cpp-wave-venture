#!/bin/bash

cmake --build ./build --parallel 16 || exit
cd ./build || exit
echo "----- running test-wave-venture -----"
./test-wave-venture
