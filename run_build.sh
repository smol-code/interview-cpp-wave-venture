#!/bin/bash

cmake -DCMAKE_BUILD_TYPE=Debug -S ./ -B ./build || exit
cmake --build ./build --parallel 16 || exit
cd ./build || exit
echo "----- running test-wave-venture -----"
./test-wave-venture
