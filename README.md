# interview-cpp-wave-venture

## Specifications
The basic requirement for this was to split vectors based on if they are "dry" or "wet", that is, if they are above or below the z axis.

The code was correct but I made the assumption that the output co-ordinates don't need to be converted back into tris and quads.



## Self/meta notes
- At first I somewhat misunderstood the data/requirement and assumed all quads are either fully below or above Z.
- I did get some guidance from a friend to look into quadratic parameterisation. My math knowledge isn't great.
- I made the (possible very incorrect) assumption that the output paths don't need to be quads.
- - I theoretically know how to deconstruct the non-quads but I'm a little stumped with trying to implement it, and my math isn't great.
- I haven't actually used RapidJSON before
- - I believe some precision may be lost even with `rapidjson::kParseFullPrecisionFlag`, but I've not tested if it does happen.
- I experimented with vectors vs unions+arrays, see [here](./docs/vec-vs-union-array.md), just for fun. So parts of the code are more littered than they should be.
- See `./output_[dry/wet].json` for the raw output from the `test-wave-venture` C++ project. The `-pretty` prefix is beautified out-of-code.



## Install and Build
Requirements:
- CMake
- C++ 17 compiler. I'm using WSL:
- - `GCC 13.1.1 x84_64-pc-linux-gnu`
- - `Clang 15.0.7 x84_64-pc-linux-gnu`
- - MSVC not tested but I saw no compatibility issues listed in the lib/s I'm using.


Running `./run_build.sh` _should_ just work, but effectively all you need to do is:
- `> mkdir build`
- `> cd build`
- `> cmake ..`
- `> cmake . --build`
- `> ./test-wave-venture`



## Style guides etc.
Using clang-format and clang-tidy, rules are just copied from my other project



## Libraries
Header-only libraries are placed in `./include`. I considered `./third_party/header_only` but that's probably a little whacky.


Libraries are copied as a backup to `./docs/vendor/` dir.

Libraries used:
- [RapidJSON](https://github.com/Tencent/rapidjson) v1.1.0
- - Using as a header-only library, but can be installed to be included with `find_package(RapidJSON)`
- - Justification: https://github.com/miloyip/nativejson-benchmark



### Notes
I'm setting the [SYSTEM](https://cmake.org/cmake/help/v3.25/prop_tgt/SYSTEM.html) property on the includes to avoid warnings from third party code.
- I don't know if this has any other implications; I just thought I'd test it out.



## License
MIT.

Please note that `bin/jsonchecker/` within RapidJSON is under the JSON license, but it's not used.

See files for any exclusions to the MIT license, e.g. `./include/rapidjson/msinttypes/*`.
